package br.com.ozcorp;

/**
 * 
 * @author Giulia Lopes
 * Programa de controle de funcion�rios da empresa Ozcorp
 *
 */
public class EmpresaTestDrive {
	public static void main(String[] args) {
	
        // Dados do funcion�rio
		System.out.println("                             Dados dos funcion�rios                                                   ");
		
		// Diretora Financeiro - Carla Ribeiro
		Funcionario carla = new Funcionario("Carla Ribeiro", "1234465", "125332211", "543", "carla@ozcorp.com.br", "143!",
				Sexo.FEMININO, TipoSanguineo.A1, new Cargo("Diretora", 3000, 0, new Departamento("Financeiro", "DF")));
		System.out.println("======================================================================================================");
		System.out.println("********** Nome: " + carla.getNome());
		System.out.println("********** CPF: " + carla.getCpf());
		System.out.println("********** RG: " + carla.getRg());
		System.out.println("********** Matricula: " + carla.getMatricula());
		System.out.println("********** E-mail: " + carla.getEmail());
		System.out.println("********** Senha: " + carla.getSenha());
		System.out.println("********** Cargo: " + carla.getCargo().getTitulo());
		System.out.println("********** Sal�rio Base: " + carla.getCargo().getSalario());
		System.out.println("********** N�vel de Acesso: " + carla.getCargo().getNivelAcesso());
		System.out.println("********** Departamento: " + carla.getCargo().getDepartamento().getNome());
		System.out.println("********** Sigla do Departamento: " + carla.getCargo().getDepartamento().getSigla());
		System.out.println("********** Tipo Sanguineo: " + carla.getTipoSanguineo().tipo);
		System.out.println("********** Sexo: " + carla.getSexo().s);
		System.out.println("======================================================================================================");
		
		// Secret�rio Administrativo - 
		Funcionario felipe = new Funcionario("Felipe Oliveira", "4352512", "464562112", "876", "felipeoli@ozcorp.com.br", "olamundo!",
				Sexo.MASCULINO, TipoSanguineo.B2, new Cargo("Secret�rio", 900, 1, new Departamento("Administrativo", "DA")));
		System.out.println("======================================================================================================");
		System.out.println("********** Nome: " + felipe.getNome());
		System.out.println("********** CPF: " + felipe.getCpf());
		System.out.println("********** RG: " + felipe.getRg());
		System.out.println("********** Matricula: " + felipe.getMatricula());
		System.out.println("********** E-mail: " + felipe.getEmail());
		System.out.println("********** Senha: " + felipe.getSenha());
		System.out.println("********** Cargo: " + felipe.getCargo().getTitulo());
		System.out.println("********** Sal�rio Base: " + felipe.getCargo().getSalario());
		System.out.println("********** N�vel de Acesso: " + felipe.getCargo().getNivelAcesso());
		System.out.println("********** Departamento: " + felipe.getCargo().getDepartamento().getNome());
		System.out.println("********** Sigla do Departamento: " + felipe.getCargo().getDepartamento().getSigla());
		System.out.println("********** Tipo Sanguineo: " + felipe.getTipoSanguineo().tipo);
		System.out.println("********** Sexo: " + felipe.getSexo().s);
		System.out.println("======================================================================================================");
		
		// Gerente de Recursos Humanos - Hilda Gouveia 
		Funcionario hilda = new Funcionario("Hilda Gouveia", "45342411", "65421134", "369", "hildagerente@ozcorp.com.br", "soulinda1243",
				Sexo.FEMININO, TipoSanguineo.AB1, new Cargo("Gerente", 1300, 2, new Departamento("Recursos Humanos", "DRH")));
		System.out.println("======================================================================================================");
		System.out.println("********** Nome: " + hilda.getNome());
		System.out.println("********** CPF: " + hilda.getCpf());
		System.out.println("********** RG: " + hilda.getRg());
		System.out.println("********** Matricula: " + hilda.getMatricula());
		System.out.println("********** E-mail: " + hilda.getEmail());
		System.out.println("********** Senha: " + hilda.getSenha());
		System.out.println("********** Cargo: " + hilda.getCargo().getTitulo());
		System.out.println("********** Sal�rio Base: " + hilda.getCargo().getSalario());
		System.out.println("********** N�vel de Acesso: " + hilda.getCargo().getNivelAcesso());
		System.out.println("********** Departamento: " + hilda.getCargo().getDepartamento().getNome());
		System.out.println("********** Sigla do Departamento: " + hilda.getCargo().getDepartamento().getSigla());
		System.out.println("********** Tipo Sanguineo: " + hilda.getTipoSanguineo().tipo);
		System.out.println("********** Sexo: " + hilda.getSexo().s);
		System.out.println("======================================================================================================");
		
		// Engenheiro Operacional - Guilherme Almeida
		Funcionario guilherme = new Funcionario("Guilherme Almeida", "1245432", "78674732", "432", "guilherme_almeida@ozcorp.com.br", "gui12348765",
				Sexo.MASCULINO, TipoSanguineo.O2, new Cargo("Engenheiro", 1000, 3, new Departamento("Operacional", "DO")));
		System.out.println("======================================================================================================");
		System.out.println("********** Nome: " + guilherme.getNome());
		System.out.println("********** CPF: " + guilherme.getCpf());
		System.out.println("********** RG: " + guilherme.getRg());
		System.out.println("********** Matricula: " + guilherme.getMatricula());
		System.out.println("********** E-mail: " + guilherme.getEmail());
		System.out.println("********** Senha: " + guilherme.getSenha());
		System.out.println("********** Cargo: " + guilherme.getCargo().getTitulo());
		System.out.println("********** Sal�rio Base: " + guilherme.getCargo().getSalario());
		System.out.println("********** N�vel de Acesso: " + guilherme.getCargo().getNivelAcesso());
		System.out.println("********** Departamento: " + guilherme.getCargo().getDepartamento().getNome());
		System.out.println("********** Sigla do Departamento: " + guilherme.getCargo().getDepartamento().getSigla());
		System.out.println("********** Tipo Sanguineo: " + guilherme.getTipoSanguineo().tipo);
		System.out.println("********** Sexo: " + guilherme.getSexo().s);
		System.out.println("======================================================================================================");
		
		// Analista Comercial - Miguel Castro
		Funcionario miguel = new Funcionario("Miguel Castro", "154876365", "858663261", "1333", "miguelcastro@ozcorp.com.br", "dragonzzzz",
				Sexo.MASCULINO, TipoSanguineo.B1, new Cargo("Analista", 950, 4, new Departamento("Comercial", "DC")));
		System.out.println("======================================================================================================");
		System.out.println("********** Nome: " + miguel.getNome());
		System.out.println("********** CPF: " + miguel.getCpf());
		System.out.println("********** RG: " + miguel.getRg());
		System.out.println("********** Matricula: " + miguel.getMatricula());
		System.out.println("********** E-mail: " + miguel.getEmail());
		System.out.println("********** Senha: " + miguel.getSenha());
		System.out.println("********** Cargo: " + miguel.getCargo().getTitulo());
		System.out.println("********** Sal�rio Base: " + miguel.getCargo().getSalario());
		System.out.println("********** N�vel de Acesso: " + miguel.getCargo().getNivelAcesso());
		System.out.println("********** Departamento: " + miguel.getCargo().getDepartamento().getNome());
		System.out.println("********** Sigla do Departamento: " + miguel.getCargo().getDepartamento().getSigla());
		System.out.println("********** Tipo Sanguineo: " + miguel.getTipoSanguineo().tipo);
		System.out.println("********** Sexo: " + miguel.getSexo().s);
		System.out.println("======================================================================================================");
		
		
		
	}
}
